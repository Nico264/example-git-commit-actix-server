use actix_web::{HttpServer, App, HttpResponse, web, post, Responder};
use serde::{Deserialize, Serialize};
use git2::{Repository, Signature, Direction};
use std::sync::Mutex;
use std::fs;
use std::path::Path;

#[derive(Serialize, Deserialize, Debug)]
struct Info {
    username: String,
}

struct AppData {
    repo: Mutex<Repository>,
    name: String,
    email: String,
}

#[post("/")]
fn set(info: web::Json<Info>, data: web::Data<AppData>) -> impl Responder {
    let info = &(*info);

    let mut repo = data.repo.lock().unwrap();
    let mut index = repo.index().unwrap();
    let head = repo.head().unwrap();

    let dir = repo.workdir().unwrap();
    let file = dir.join("foo.txt");

    if !fs::write(&file, ron::ser::to_string(info).unwrap()).is_ok() {
        return HttpResponse::InternalServerError()
    }

    index.add_path(Path::new("foo.txt")).unwrap();

    let sig = Signature::now(&data.name, &data.email).unwrap();

    let oid = index.write_tree().unwrap();
    let tree = repo.find_tree(oid).unwrap();

    let parent_commit = head.peel_to_commit().unwrap();

    if !repo.commit(Some("HEAD"), &sig, &sig, "Test", &tree,  &[&parent_commit]).is_ok() {
        return HttpResponse::InternalServerError()
    }

    let mut remote = repo.find_remote("origin").unwrap();

    if !remote.connect(Direction::Push).is_ok() {
        return HttpResponse::InternalServerError()
    }

    match remote.push(&["refs/heads/master:refs/heads/master"], None) {
        Ok(id) => HttpResponse::Ok(),
        Err(e) => HttpResponse::InternalServerError(),
    }
}

fn main() {
    let data = web::Data::new(AppData {
        repo: Mutex::new(
            match Repository::open("/home/nico/test-git") {
                Ok(repo) => repo,
                Err(e) => panic!("failed to open: {}", e),
            }
        ),
        name: String::from("Test"),
        email: String::from("test@test.test"),
    });

    {
        let repo = &data.repo;
        let repo = repo.lock().unwrap();
        repo.index().expect("No index");
        let head = repo.head().expect("No head");
        head.peel_to_commit().expect("HEAD does not point to a commit");
        repo.workdir().expect("This is a bare repo");
        repo.find_remote("origin").expect("No remote named origin");
    }

    HttpServer::new(move || App::new().register_data(data.clone()).service(set))
        .bind("127.0.0.1:8088")
        .unwrap()
        .run()
        .unwrap();
}
